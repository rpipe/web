* Overview

*Rpipe* is a simple tool for forwarding the content of a local file to
the stdin of a program running on a remote host via TCP. The tool was
created as a _prog_ mailer to deliver messages to a docker container 
running GNU Mailman 2.x[1].

*Rpipe* implements a slightly modified version of TCPMUX[2] protocol. The same
binary (=rpipe=) serves both as a server (on the remote end) and as a
client (on the local end). See the *Example* section below.

* Istallation

To build and install the packaged version, the usual procedure applies:

#+BEGIN_SRC shell-script
   ./configure
   make
   sudo make install
#+END_SRC

There is a number of options that modify the behaviod of *configure*.  The
most important of them is =--prefix=, which sets the installation prefix
directory.  By default, that directory is =/usr/local=, which means
that the rpipe binary will be installed in =/usr/local/bin=.  To
install it in =/usr/bin=, run

#+BEGIN_SRC shell-script
   ./configure --prefix=/usr
#+END_SRC

For a detailed description of the installation procedure, refer to the
[[https://www.gnu.org/savannah-checkouts/gnu/autoconf/manual/autoconf-2.70/html_node/Running-configure-Scripts.html][documentation of configure]].

* Downloads

Packaged releases of *rpipe* are available for download from
https://download.gnu.org.ua/release/rpipe/.

The recent releases are:

- [[https://download.gnu.org.ua/release/rpipe/rpipe-1.8.tar.gz][rpipe-1.8.tar.gz]] ([[https://download.gnu.org.ua/release/rpipe/rpipe-1.8.tar.gz.sig][sig]]), 2021-05-17 ::
  Checksums (MD5, SHA1):
  #+BEGIN_SRC shell-script
  6e2edc913e4b78f1272d3831ac67d287  rpipe-1.8.tar.gz
  534794957e8f4e4b89eda40984a25be0579c340e  rpipe-1.8.tar.gz
  #+END_SRC

- [[https://download.gnu.org.ua/release/rpipe/rpipe-1.7.tar.gz][rpipe-1.7.tar.gz]] ([[https://download.gnu.org.ua/release/rpipe/rpipe-1.7.tar.gz.sig][sig]]), 2021-05-12 ::
  Checksums (MD5, SHA1):
  #+BEGIN_SRC shell-script
  848f3ccf11df8f4b7a16e83dd4941f6e  rpipe-1.7.tar.gz
  8935213d7b8d62302fc15567279896ebfff22b16  rpipe-1.7.tar.gz
  #+END_SRC

- [[https://download.gnu.org.ua/release/rpipe/rpipe-1.6.tar.gz][rpipe-1.6.tar.gz]] ([[https://download.gnu.org.ua/release/rpipe/rpipe-1.6.tar.gz.sig][sig]]), 2021-05-09 ::
  Checksums (MD5, SHA1):
  #+BEGIN_SRC shell-script
  9dab739e79cbb386821f8b290aabd0ac  rpipe-1.6.tar.gz
  730939e1ea8db26dedfdd163776918a38910e518  rpipe-1.6.tar.gz
  #+END_SRC

Use the PGP signature files (marked as =sig= in the listing above) to
verify that the corresponding tarball is intact.  First, import the
author's PGP key:

#+BEGIN_SRC shell-script
gpg --keyserver keys.gnupg.net --recv-keys 3602B07F55D0C732
#+END_SRC

Then, run the following command:

#+BEGIN_SRC shell-script
gpg --verify rpipe-1.5.tar.gz.sig
#+END_SRC

* Example

The following configuration allows the system administrator to run
mailman 2.x in a docker container and to distribute messages via the
usual list mechanism using MTA on the host machine.

** Server configuration

The container exposes TCP port 1 and runs the following command:

#+BEGIN_SRC shell-script
 /usr/bin/rpipe -s -vv -udaemon -gsmmsp /opt/mailman/mail/mailman
#+END_SRC

The =-s= option tells it to run as a server. The two =-v= options select
maximal output verbosity. Diagnostics will be issued to the standard
error. The =-u= and =-g= options specify the user and group to run mailman
as. The only non-optional argument supplies the full pathname to the
binary to run when a request is accepted from the client.

For a detailed description of =rpipe= command line options, see [[http://man.gnu.org.ua/1/rpipe][rpipe]](1).

The =rpipe= utility runs in foreground.

** Client configuration

Normally Mailman-based mailing lists are configured as follows (using
sendmail-style aliases):

#+BEGIN_SRC
 some-list:       "|/opt/mailman/mail/mailman post some-list"
 some-list-admin: "|/opt/mailman/mail/mailman admin some-list"
 ...
#+END_SRC

Just replace the program name after the pipe symbol, so that your list setup
becomes:

#+BEGIN_SRC
 some-list:       "|/usr/bin/rpipe -a 172.17.0.2 post some-list"
 some-list-admin: "|/usr/bin/rpipe -a 172.17.0.2 admin some-list"
 ...
#+END_SRC

Replace 172.17.0.2 with the actual IP of the container.

* Documentation

The documentaion for *rpipe* is [[http://man.gnu.org.ua/1/rpipe][available online]].

* Other references:

- [[https://puszcza.gnu.org.ua/projects/rpipe][Project development page]]

- [[http://git.gnu.org.ua/cgit/rpipe.git][Git repository]]

* Bug reports

If you think you found a bug in *rpipe* or in its documentation, please 
send a mail to [[mailto:gray@gnu.org][Sergey Poznyakoff]] or use the web [[https://puszcza.gnu.org.ua/bugs/?group=rpipe][bug tracker]].

* License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program.  If not, see <http://www.gnu.org/licenses/>.

* Footnotes

[1] https://www.list.org
[2] https://tools.ietf.org/html/rfc1078

* Document settings :noexport:

Please ignore this section. It supplies the variables necessary for
proper rendering of this document.

:PROPERTIES:
:VISIBILITY: folded
:END:

#+TITLE: rpipe
#+STARTUP: showall
#+EXCLUDE_TAGS: noexport
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="style.css" />
#+OPTIONS: ^:nil

# Local Variables:
# mode: org
# paragraph-separate: "[ 	^L]*$"
# version-control: never
# End:
